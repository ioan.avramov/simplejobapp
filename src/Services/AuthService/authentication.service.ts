import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/_userModel';
import { UserDataService } from '../userService/user-data.service';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;

    constructor(private userService: UserDataService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    }

    public get currentUserValue(): User {
        if (localStorage.length > 0){
          const localStorageItem: User[] = JSON.parse((localStorage.getItem('currentUser')));
         // console.log('Current logged user: ' + JSON.stringify(localStorageItem[0]));
          this.currentUserSubject = new BehaviorSubject<User>(localStorageItem[0]);
          return this.currentUserSubject.value;
        }
        return null;
    }

    login(email: string, password: string): Observable<User> {
      return this.userService.getUser(email, password);
    }

    logout(): void {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
