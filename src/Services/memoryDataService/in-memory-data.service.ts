import { Injectable, Type } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Categoriy } from 'src/app/_offerModel';
import { Role, User } from 'src/app/_userModel';


@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const users = [
      {
        id: 1,
        firstName: 'Ioan',
        lastName: 'Avramov',
        password: 'ioanpass',
        email: 'ioan.avramov@somedomain.com',
        role: Role.organization
      },
      {
        id: 2,
        firstName: 'Georgi',
        lastName: 'Georgiev',
        password: 'georgipass',
        email: 'georgi.georgiev@somedomain.com',
        role: Role.regular
      },
      {
        id: 3,
        firstName: 'Spas',
        lastName: 'Spasov',
        password: 'spaspass',
        email: 'spas.spasov@somedomain.com',
        role: Role.regular
      },
      {
        id: 4,
        firstName: 'Petur',
        lastName: 'Petrov',
        password: 'peturpass',
        email: 'petur.petrov@somedomain.com',
        role: Role.regular
      },
      {
        id: 5,
        firstName: 'Martin',
        lastName: 'Hristov',
        password: 'martinpass',
        email: 'martin.hristov@somedomain.com',
        role: Role.regular
      }
    ];
    const offers = [
      {
         id: 1,
         title: 'Google',
         content: 'This is some content for a job that i cannot make up',
         likes: 0,
         type: Type['Full-time'],
         category: 'Full-Stack',
         applicants: [users[2], users[3]],
         organization: users[1]
      },
      {
        id: 2,
        title: 'Facebook',
        content: 'This is some content for a job that i cannot make up',
        likes: 0,
        type: Type['Paty-time'],
        category: Categoriy.DevOps,
        applicants: [users[3], users[4]],
        organization: users[2]
      },
      {
        id: 3,
        title: 'Twitter',
        content: 'This is some content for a job that i cannot make up',
        likes: 0,
        type: Type['Full-time'],
        category: Categoriy['Junior-developer'],
        applicants: [users[2], users[4]],
        organization: users[3]
      }
    ];
    console.log('creation successfull');
    return {users, offers};
  }
  genId(users: User[]): number {
    return users.length > 0 ? Math.max(...users.map(user => user.id)) + 1 : 11;
  }
}
