import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Offer } from 'src/app/_offerModel';

@Injectable({
  providedIn: 'root'
})
export class OfferDataService {

  public storeOfferData: Offer;

  constructor(private http: HttpClient) { }

  private offersUrl = 'http://localhost:3000/offers';

  getOffers(): Observable<Offer[]> {
    return this.http.get<Offer[]>(this.offersUrl);
  }
  getOfferById(id: number): Observable<Offer>{
    return this.http.get<Offer>(`${this.offersUrl}/${id}`);
  }
  updateOffer(off: Offer): Observable<Offer>{
    return this.http.put<Offer>(`${this.offersUrl}/${off.id}`, off);
  }
  createOffer(off: Offer): Observable<Offer>{
    return this.http.post<Offer>(this.offersUrl, off);
  }
  deleteOffer(id: number): Observable<Offer>{
    return this.http.delete<Offer>(`${this.offersUrl}/${id}`);
  }
}
