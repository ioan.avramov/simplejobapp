
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/_userModel';


@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  private currentUserSubject: BehaviorSubject<User>;
  constructor(
    private http: HttpClient) {  }

    public storePublisherData: User;

    private usersUrl = 'http://localhost:3000/users';

    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    getUsers(): Observable<User[]>{
      console.log('requesting all users');
      return this.http.get<User[]>(this.usersUrl);
    }

    getUser(email: string , password: string ): Observable<User>{
     const params = new HttpParams().set('email', email).set('password', password);
     return this.http.get<User>(this.usersUrl, {params});
    }

    getUserById(id: number): Observable<User>{
      return this.http.get<User>(`${this.usersUrl}/${id}`);
    }

    addUser(user: User): Observable<User> {
      console.log('adding an user');
      return this.http.post<User>(this.usersUrl, user);
    }

    updateUser(user: User): Observable<User>{
      return this.http.put<User>(`${this.usersUrl}/${user.id}`, user, this.httpOptions);
    }
    deleteUser(user: User): Observable<User>{
      return this.http.delete<User>(`${this.usersUrl}/${user.id}`);
    }
  }
