import { User } from './../_userModel/user';

export class Offer{
  id?: number;
  title: string;
  content: string;
  likes?: number;
  type: Type;
  category: Categoriy;
  applicants_ids?: number[];
  uploader_id: number;
  approved_user_id?: number;
  offer_status: string;
}
export enum Type{
  fulltime = 'Full-time',
  parttime = 'Part-time',
  remote = 'Remote'
}
export enum Categoriy{
  officeAdministration = 'Office administration',
  development = 'Development',
  fullStack = 'Full-stack',
  seniorDeveloper = 'Senior-developer',
  junioDeveloper = 'Junior-developer',
  softwareEngineer = 'Sowfware engineer',
  devops = 'DevOps',
  humanResources = 'Human Resources',
  salesRepresentative = 'Sales Representative',
  forntEnd = 'Front-end developer',
  backend = 'Back-end developer'

}
export enum Status{
  Active = 'active',
  Inactive = 'inactive'
}
