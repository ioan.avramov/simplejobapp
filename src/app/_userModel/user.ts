import { Offer } from './../_offerModel/offer';
export class User {
  id?: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  role: string;
  account_status?: string;
  liked_posts?: number[];
  applied_offers_id?: number[];
  rejected_offers_id?: number[];
  accepted_offers_id?: number[];
}
export enum Role{
  organization = 'Organization',
  regular = 'Regular'
}
export enum AccountStatus{
  active = 'active',
  disabled = 'disabled'
}
