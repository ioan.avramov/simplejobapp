import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/Services/AuthService/authentication.service';
import { User } from './_userModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(){ }

  title = 'ng-final-proj';
}
