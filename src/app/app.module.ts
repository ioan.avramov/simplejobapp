import { FooterComponent } from './footer/footer.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent, routerConfig } from './header/header.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OfferComponent } from './offer/offer.component';
import { OffersViewComponent } from './offers-view/offers-view.component';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { RegisterComponent } from './register/register.component';
import { OfferDetailsComponent } from './offer-details/offer-details.component';
import { NewOfferComponent } from './new-offer/new-offer.component';
import { OfferUpdateViewComponent } from './offer-update-view/offer-update-view.component';
import { FieldErrorMessageComponent } from './field-error-message/field-error-message.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    LoginComponent,
    OfferComponent,
    OffersViewComponent,
    MyaccountComponent,
    RegisterComponent,
    OfferDetailsComponent,
    NewOfferComponent,
    OfferUpdateViewComponent,
    FieldErrorMessageComponent
   ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routerConfig),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
