import { OfferUpdateViewComponent } from './../offer-update-view/offer-update-view.component';
import { OfferDetailsComponent } from './../offer-details/offer-details.component';
import { RegisterComponent } from './../register/register.component';
import { MyaccountComponent } from './../myaccount/myaccount.component';
import { OffersViewComponent } from './../offers-view/offers-view.component';
import { OfferComponent } from './../offer/offer.component';
import { AppComponent } from './../app.component';
import { MainComponent } from './../main/main.component';
import { LoginComponent } from './../login/login.component';
import { Component, OnInit } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { User } from '../_userModel';
import { AuthenticationService } from 'src/Services/AuthService/authentication.service';
import { NewOfferComponent } from '../new-offer/new-offer.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{

  loggedUser: User;

  constructor(
    private router: Router,
    private authentication: AuthenticationService,
  ) { }

  ngOnInit() {
    if (!this.userValidation()){
      this.router.navigate(['login']);
      return;
    }
    this.router.navigate(['/']);
  }

  userValidation(): boolean{
    this.loggedUser = this.authentication.currentUserValue;
    if (this.loggedUser === null){
      console.log('there is no user so i am giving false');
      return false;
    }
    return true;
  }

  backToHomeAction(): void{
    this.router.navigate(['/']);
  }
  goToOffersAction() {
    if (!this.userValidation()){
      console.log('navigating to login');
      this.router.navigate(['login']);
      return;
    }
    console.log('I am  asking for offers');
    this.router.navigate(['offers']);
  }
  myAccuntRedirect(){
    if (!this.userValidation()){
      console.log('navigating to login');
      this.router.navigate(['login']);
      return;
    }
    this.router.navigate(['myaccount']);
  }

}
export const routerConfig: Routes = [
  {
      path: 'login',
      component: LoginComponent
  },
  {
    path: 'offers',
    component: OffersViewComponent
  },
  {
    path: 'myaccount',
    component: MyaccountComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'offerdetails',
    component: OfferDetailsComponent
  },
  {
    path: 'newoffer',
    component: NewOfferComponent
  },
  {
    path: 'offerupdate',
    component: OfferUpdateViewComponent
  }

];
