import { AccountStatus, Role } from './../_userModel/user';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/Services/AuthService/authentication.service';
import { User } from '../_userModel';

@Component({ templateUrl: 'login.component.html' })

export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    invalidUserDetails = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit(): void{
       this.buildForm();
    }

    // convenience getter for easy access to form fields
    get f(): any { return this.loginForm.controls; }

    onSubmit(): void {
        this.submitted = true;
        this.loading = true;

        this.authenticationService.login(this.f.email.value, this.f.password.value).subscribe((res) => {
          if (res[0] === undefined){
            this.invalidUserDetails = true;
            this.loading = false;
            this.buildForm();
            return;
          }
          else if (res[0].role === 'Organization' && res[0].account_status === 'disabled'){
            this.invalidUserDetails = true;
            this.loading = false;
            this.buildForm();
            return;
          }
          localStorage.setItem('currentUser', JSON.stringify(res));
          this.router.navigate(['myaccount']);
        });
    }
    redirectToRegister(){
      this.router.navigate(['register']);
    }
    buildForm(): void {
      this.loginForm = this.formBuilder.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
    });
    }
}
