import { Status } from './../_offerModel/offer';
import { Role, AccountStatus } from './../_userModel/user';
import { Router } from '@angular/router';
import { OfferDataService } from 'src/Services/offerService/offer-data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/Services/AuthService/authentication.service';
import { User } from '../_userModel';
import { UserDataService } from 'src/Services/userService/user-data.service';
import { Offer } from '../_offerModel';
@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {

  accountForm: FormGroup;
  loggedUser: User;
  rejectedOffers: Offer[] = [];
  approvedOffers: Offer[] = [];
  prendingOffers: Offer[] = [];

  rejectedLoaded = false;
  approvedLoaded = false;
  pendingLoaded = false;

  constructor(
    private authenticate: AuthenticationService,
    private formBuilder: FormBuilder,
    private userService: UserDataService,
    private offerService: OfferDataService,
    private router: Router
    ) { }

  get f(): any { return this.accountForm.controls; }

  ngOnInit(): void {
    this.loggedUser = this.authenticate.currentUserValue;
    this.setInputs();
    if (this.loggedUser.role === 'Regular'){
      this.loadAcceptedOffers();
      this.loadRejectedoffers();
      this.loadPendingApplications();
    }
  }
  updateAccount(): void {
    let updatedUser: User;
    if (this.f.password.value === this.loggedUser.password) {
      if ( this.f.newpassword.value === '' ){
        updatedUser = {
          id: this.loggedUser.id,
          firstName: this.f.firstName.value,
          lastName: this.f.lastName.value,
          email: this.f.email.value,
          role: this.loggedUser.role,
          liked_posts: this.loggedUser.liked_posts,
          password: this.loggedUser.password,
          applied_offers_id: this.loggedUser.applied_offers_id,
          rejected_offers_id: this.loggedUser.rejected_offers_id,
          accepted_offers_id: this.loggedUser.accepted_offers_id
        };
      }
      else if ( !(this.f.newpassword.value === '') && this.passwordCheck()){
        updatedUser = {
          id: this.loggedUser.id,
          firstName: this.f.firstName.value,
          lastName: this.f.lastName.value,
          email: this.f.email.value,
          role: this.loggedUser.role,
          liked_posts: this.loggedUser.liked_posts,
          password: this.f.newpassword.value,
          applied_offers_id: this.loggedUser.applied_offers_id,
          rejected_offers_id: this.loggedUser.rejected_offers_id,
          accepted_offers_id: this.loggedUser.accepted_offers_id
        };
      }
    }
    if (this.accountForm.valid && updatedUser){
      this.userService.updateUser(updatedUser).subscribe(() => {
        this.userService.getUserById(this.loggedUser.id).subscribe((u) => {
          localStorage.setItem('currentUser', JSON.stringify(Array(u)));
          this.loggedUser = u;
          this.setInputs();
        });
      });
    }
  }
  setInputs(): void {
    this.accountForm = this.formBuilder.group({
      firstName: [this.loggedUser.firstName],
      lastName: [this.loggedUser.lastName],
      email: [this.loggedUser.email, Validators.required],
      role: [this.loggedUser.role],
      password: ['', Validators.required],
      newpassword: [''],
      confirmpassword: ['']
    });
  }

  matchCheck(): void{
    if (this.passwordCheck()){
      console.log('passwords match');
      document.getElementById('cnp_lbl').style.color = 'green';
    }else {
      console.log('dont match');
      document.getElementById('cnp_lbl').style.color = 'red';
    }
  }
  passwordCheck(): boolean{
    if (this.f.newpassword.value === this.f.confirmpassword.value){
      return true;
    }
    return false;
  }

  onLogoutClick(): void{
    localStorage.clear();
    this.router.navigate(['login']);
  }

  loadAcceptedOffers(): void {
    this.loggedUser.accepted_offers_id.forEach((offerId) => {
      this.offerService.getOfferById(offerId).subscribe((response) => {
        this.approvedOffers.push(response);
        this.approvedLoaded = true;
      });
    });
  }


  loadRejectedoffers(): void {
    this.loggedUser.rejected_offers_id.forEach((offerId) => {
      this.offerService.getOfferById(offerId).subscribe((response) => {
        this.rejectedOffers.push(response);
        this.rejectedLoaded = true;
      });
    });
  }

  loadPendingApplications(): void {
    this.loggedUser.applied_offers_id.forEach((offerId) => {
      this.offerService.getOfferById(offerId).subscribe((response) => {
        this.prendingOffers.push(response);
        this.pendingLoaded = true;
      });
    });
  }
  redirectToOffer(off: Offer): void{
    this.offerService.storeOfferData = off;
    this.userService.getUserById(off.uploader_id).subscribe((res) => {
      this.userService.storePublisherData = res;
      this.router.navigate(['offerdetails']);
    });
  }

  onDeleteAccountClick(): void {
    if (this.loggedUser.role === Role.organization){
      this.offerService.getOffers().subscribe((offers) => {
        offers.forEach((offer) => {
          if (offer.uploader_id === this.loggedUser.id && offer.offer_status === Status.Active){
            offer.offer_status = Status.Inactive;
            this.normalizeUserOffers(offer);
            this.offerService.updateOffer(offer).subscribe();
          }
        });
      });
      this.loggedUser.account_status = AccountStatus.disabled;
      this.userService.updateUser(this.loggedUser).subscribe();
      localStorage.clear();
      this.router.navigate(['login']);
    }
    else if (this.loggedUser.role === Role.regular ){
      this.offerService.getOffers().subscribe((offers) => {
        offers.forEach((offer) => {
          if (offer.applicants_ids.includes(this.loggedUser.id)){
            const index = offer.applicants_ids.indexOf(this.loggedUser.id, 0);
            if (index > -1) {
              offer.applicants_ids.splice(index, 1);
            }
            if (offer.approved_user_id){
              if (offer.approved_user_id === this.loggedUser.id){
                offer.offer_status = Status.Active;
                offer.approved_user_id = null;
              }
            }
            this.offerService.updateOffer(offer).subscribe();
          }
        });
      });
      this.userService.deleteUser(this.loggedUser).subscribe();
      localStorage.clear();
      this.router.navigate(['login']);
    }
  }



  normalizeUserOffers(offer: Offer): void {
    offer.applicants_ids.forEach((userId) => {
      this.userService.getUserById(userId).subscribe((user) => {
        const index = user.applied_offers_id.indexOf(offer.id, 0);
        if (index > -1) {
          user.applied_offers_id.splice(index, 1);
        }
        this.userService.updateUser(user).subscribe();
      });
    });
  }
}
