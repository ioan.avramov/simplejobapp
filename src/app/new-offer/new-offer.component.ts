import { Router } from '@angular/router';
import { AuthenticationService } from './../../Services/AuthService/authentication.service';
import { OfferDataService } from 'src/Services/offerService/offer-data.service';
import { Categoriy, Offer, Status } from './../_offerModel/offer';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Type } from '../_offerModel';

@Component({
  selector: 'app-new-offer',
  templateUrl: './new-offer.component.html',
  styleUrls: ['./new-offer.component.css']
})
export class NewOfferComponent implements OnInit {

  offerForm: FormGroup;
  types: Type[];
  categories: Categoriy[];

  constructor(private formBuilder: FormBuilder,
              private offerService: OfferDataService,
              private auth: AuthenticationService,
              private router: Router) { }

  ngOnInit(): void {

    this.offerForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      type: ['', Validators.required],
      category: ['', Validators.required]
    });
    //const keys = Object.keys(Type);
    this.types = Object.keys(Type).map(key => Type[key]);
    this.categories = Object.keys(Categoriy).map(key => Categoriy[key]);

  }

  get f(): any { return this.offerForm.controls; }

  onSubmit(): void{

    const newOffer: Offer = {
      title: this.f.title.value,
      type: this.f.type.value,
      category: this.f.category.value,
      content: this.f.content.value,
      uploader_id: this.auth.currentUserValue.id,
      offer_status: Status.Active,
      applicants_ids: [],
      likes: 0
    };
    this.offerService.createOffer(newOffer).subscribe();
    this.router.navigate(['offers']);
  }
}
