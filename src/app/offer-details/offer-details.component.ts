import { Status } from './../_offerModel/offer';

import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/Services/AuthService/authentication.service';
import { OfferDataService } from 'src/Services/offerService/offer-data.service';
import { UserDataService } from 'src/Services/userService/user-data.service';
import { Offer } from '../_offerModel';
import { User } from '../_userModel';
import { repeat } from 'rxjs/operators';

@Component({
  selector: 'app-offer-details',
  templateUrl: './offer-details.component.html',
  styleUrls: ['./offer-details.component.css']
})
export class OfferDetailsComponent implements OnInit {

  offer: Offer;
  publisher: User;
  currUser: User;
  applicants: User[] = [];
  applicantsFilled = false;
  viewerIsOrganizator = false;
  currUserHasApplied = false;
  approvedApplicant = false;


  constructor(private offerService: OfferDataService,
              private userService: UserDataService,
              private auth: AuthenticationService) { }


  ngOnInit(): void {
    this.offer = this.offerService.storeOfferData;
    this.publisher = this.userService.storePublisherData;
    this.currUser = this.auth.currentUserValue;
    if (  this.currUser.id === this.publisher.id ){
      this.viewerIsOrganizator = true;
    }
    this.approvedApplicant = this.offerHasSomeoneApproved();
    this.showApplicants();
    this.applicantsCheck();
  }

  applicantsCheck(): void{
    if (this.currUser.role === 'Regular' && (this.currUser.applied_offers_id.includes(this.offer.id) ||
        this.currUser.accepted_offers_id.includes(this.offer.id) ||
        this.currUser.rejected_offers_id.includes(this.offer.id))){
      this.currUserHasApplied = true;
    }
  }

  showApplicants(): void{
    this.applicants = [];
    this.offer.applicants_ids.forEach(element => {
      this.userService.getUserById(element).subscribe(u => {
        this.applicants.push(u);
        this.applicantsFilled = true;
      });
    });
  }
  offerHasSomeoneApproved(): boolean{
    if (this.offer.approved_user_id){
      return true;
    }
    return false;
  }

// Regular user related
  sendApplication(): void{
    this.offer.applicants_ids.push(this.currUser.id);
    this.offerService.updateOffer(this.offer).subscribe();
    this.currUser.applied_offers_id.push(this.offer.id);
    this.userService.updateUser(this.currUser).subscribe();
    localStorage.setItem('currentUser', JSON.stringify(Array(this.currUser)));
    this.applicantsCheck();
  }
// Organization related

  onAcceptClick(approvedUser: User): void{
    approvedUser.accepted_offers_id.push(this.offer.id);
    const i = approvedUser.applied_offers_id.indexOf(this.offer.id, 0);
    if (i > -1){
      approvedUser.applied_offers_id.splice(i, 1);
    }
    this.userService.updateUser(approvedUser).subscribe();

    this.offer.applicants_ids.forEach(appl => {
        this.userService.getUserById(appl).subscribe((response) => {
          if (!(approvedUser.id === response.id)){
            response.rejected_offers_id.push(this.offer.id);
            const index = response.applied_offers_id.indexOf(this.offer.id, 0);
            if (index > -1){
              response.applied_offers_id.splice(index, 1);
            }
            this.userService.updateUser(response).subscribe();
          }
        });
    });

    this.offer.approved_user_id = approvedUser.id;
    this.offer.applicants_ids = [approvedUser.id];
    this.offer.offer_status = Status.Inactive;
    this.offerService.updateOffer(this.offer).subscribe();

    this.approvedApplicant = true;
    this.showApplicants();

  }
  onDeclineClick(declinedUser: User): void{

    declinedUser.rejected_offers_id.push(this.offer.id);
    const i = declinedUser.applied_offers_id.indexOf(this.offer.id, 0);
    if (i > -1){
      declinedUser.applied_offers_id.splice(i, 1);
    }
    this.userService.updateUser(declinedUser).subscribe();


    const index = this.offer.applicants_ids.indexOf(declinedUser.id, 0);
    if (index > -1) {
      this.offer.applicants_ids.splice(index, 1);
    }
    this.offerService.updateOffer(this.offer).subscribe();
    this.showApplicants();
  }



}
