import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferUpdateViewComponent } from './offer-update-view.component';

describe('OfferUpdateViewComponent', () => {
  let component: OfferUpdateViewComponent;
  let fixture: ComponentFixture<OfferUpdateViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferUpdateViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferUpdateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
