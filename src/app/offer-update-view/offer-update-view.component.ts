import { Role } from './../_userModel/user';
import { UserDataService } from './../../Services/userService/user-data.service';
import { Router } from '@angular/router';
import { Categoriy } from './../_offerModel/offer';
import { OfferDataService } from 'src/Services/offerService/offer-data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Offer, Type } from '../_offerModel';

@Component({
  selector: 'app-offer-update-view',
  templateUrl: './offer-update-view.component.html',
  styleUrls: ['./offer-update-view.component.css']
})
export class OfferUpdateViewComponent implements OnInit {

  updateForm: FormGroup;
  offer: Offer;
  idPopulated = false;
  categories: Categoriy[];
  types: Type[];

  constructor(private offerService: OfferDataService,
              private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserDataService) { }

  ngOnInit(): void {
    this.offer = this.offerService.storeOfferData;
    this.updateForm = this.formBuilder.group({
      title: [this.offer.title, [Validators.required]],
      content: [this.offer.content, [Validators.required]],
      type: [this.offer.type, [Validators.required]],
      category: [this.offer.category, [Validators.required]]
    });
    this.types = Object.keys(Type).map(key => Type[key]);
    this.categories = Object.keys(Categoriy).map(key => Categoriy[key]);
  }
  get f(): any {return this.updateForm.controls; }

  onSubmit(): void{
    this.offer.title = this.f.title.value;
    this.offer.category = this.f.category.value;
    this.offer.type = this.f.type.value;
    this.offer.content = this.f.content.value;
    this.offerService.updateOffer(this.offer).subscribe();
    this.router.navigate(['offers']);
    console.log('My delete triggers the submit');
  }

  onDeleteOfferClick(): void{
    this.userService.getUsers().subscribe((users) => {
      users.forEach((user) => {
        if (user.role === Role.regular &&  user.applied_offers_id.includes(this.offer.id)){
          const index = user.applied_offers_id.indexOf(this.offer.id, 0);
          if (index > -1) {
            user.applied_offers_id.splice(index, 1);
         }
          this.userService.updateUser(user).subscribe();
        }
      });
    });
    this.offerService.deleteOffer(this.offer.id).subscribe();
    this.router.navigate(['offers']);
  }

}
