import { Offer } from 'src/app/_offerModel';
import { Router } from '@angular/router';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../_userModel';
import { UserDataService } from 'src/Services/userService/user-data.service';
import { OfferDataService } from 'src/Services/offerService/offer-data.service';
import { AuthenticationService } from 'src/Services/AuthService/authentication.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {

  @Input() offer: Offer;

  publisher: User;
  userPopulated = false;
  loggedUser: User;
  isLiked = false;

  likeClasses: string[] = ['btn', 'btn-outline-danger', 'float-right'];

  constructor(private userService: UserDataService,
              private router: Router,
              private offerService: OfferDataService,
              private auth: AuthenticationService) { }

  ngOnInit(): void {
    this.userService.getUserById(this.offer.uploader_id)
    .subscribe((response) => {
      this.publisher = response;
      this.userPopulated = true;
    });
    this.loggedUser = this.auth.currentUserValue;
    this.setLike();
  }

  viewMore(): void{
    this.offerService.storeOfferData = this.offer;
    this.userService.storePublisherData = this.publisher;
    this.router.navigate(['offerdetails']);
  }

  clickLikeEvent(): void{
    if (!this.loggedUser.liked_posts.includes(this.offer.id)){
      this.loggedUser.liked_posts.push(this.offer.id);
      this.offer.likes++;
      console.log('Offer liked');
    }
    else {
      const index = this.loggedUser.liked_posts.indexOf(this.offer.id, 0);
      if (index > -1) {
        this.loggedUser.liked_posts.splice(index, 1);
      }
      this.offer.likes--;
      console.log('Offer disliked');
    }
    this.userService.updateUser(this.loggedUser).subscribe();
    this.offerService.updateOffer(this.offer).subscribe();
    localStorage.setItem('currentUser', JSON.stringify(Array(this.loggedUser)));
    this.setLike();
  }

  setLike(): void{
    if (this.loggedUser.role === 'Regular'){
      if (this.loggedUser.liked_posts.includes(this.offer.id)){
          this.isLiked = true;
        }
        else{
          this.isLiked = false;
        }
      this.toggleClasses();
    }
  }
  toggleClasses(): void {

    if (!this.isLiked){
      this.likeClasses.splice(this.likeClasses.length - 2, 2); // removes the classes
    }else{
      this.likeClasses.push('bg-danger');
      this.likeClasses.push('text-white');
    }
  }

  onUpdateClick(): void{
    this.offerService.storeOfferData = this.offer;
    this.router.navigate(['offerupdate']);
  }
}
