import { Routes, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Offer } from '../_offerModel';
import { OfferDataService } from 'src/Services/offerService/offer-data.service';
import { Role, User } from '../_userModel';
import { AuthenticationService } from 'src/Services/AuthService/authentication.service';


@Component({
  selector: 'app-offers-view',
  templateUrl: './offers-view.component.html',
  styleUrls: ['./offers-view.component.css']
})
export class OffersViewComponent implements OnInit {

  offers: Observable<Offer[]>;
  loggedUser: User;
  isOrganizator = false;

  constructor( private offerService: OfferDataService,
               private auth: AuthenticationService,
               private router: Router) { }

  ngOnInit(): void {
    this.offers = this.offerService.getOffers();
    this.loggedUser = this.auth.currentUserValue;
    if (this.loggedUser.role === Role.organization){
      this.isOrganizator = true;
    }
  }

  redirectToNewOfferForm(): void{
    this.router.navigate(['newoffer']);
  }

}
