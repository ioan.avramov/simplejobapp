import { Router } from '@angular/router';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Role, User } from '../_userModel';
import { UserDataService } from 'src/Services/userService/user-data.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  roles: string[];
  passwordsMatch = false;

  constructor(private fb: FormBuilder, private userService: UserDataService, private router: Router) { }

  ngOnInit(): void {

    this.registerForm = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.minLength(5)]],
      role: [Role.organization, [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      password_confirmation: ['', [Validators.required]],
    });
    this.roles = Object.keys(Role).map(key => Role[key]);
  }

  get f(): any { return this.registerForm.controls; }

  onSubmit(): void{
    if (this.passwordCheck){
      const user: User = {
        firstName: this.f.firstName.value,
        lastName: this.f.lastName.value,
        role: this.f.role.value,
        email: this.f.email.value,
        password: this.f.password.value,
        liked_posts: [],
        applied_offers_id: [],
        rejected_offers_id: [],
        accepted_offers_id: []
      };
      this.userService.addUser(user).subscribe((u) => {
        localStorage.setItem('currentUser', JSON.stringify(Array(u)));
        this.router.navigate(['myaccount']);
      });
    }
  }
  matchCheck(): void{
    if (this.passwordCheck()){
      this.passwordsMatch = true;
    }else {
      this.passwordsMatch = false;
    }
  }
  passwordCheck(): boolean{
    if (this.f.password.value === this.f.password_confirmation.value){
      return true;
    }
    return false;
  }
}
